package com.page;

import io.appium.java_client.pagefactory.AndroidBy;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.WhenPageOpens;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Set;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static io.vavr.API.$;

/**
 * Created by operador on 14/12/2016.
 */
@DefaultUrl("https://www.google.es")
//@DefaultUrl("https://www.ryanair.com/es/es")
public class RyanairSearchPage extends PageObject {

    JavascriptExecutor js = (JavascriptExecutor) this.getDriver();
    WebDriver dr = this.getDriver();
    //ATRIBUTOS
    @CacheLookup
    //COOKIES POP-UP
    @FindBy(xpath = "//*[@id=\"home\"]/cookie-pop-up/div/div[2]")
    private WebElement closePopUp;
    //ROUND OR ONE-WAY RADIO BUTTONS
    @FindBy(xpath = "//*[@id=\"lbl-flight-search-type-return\"]")
    private WebElement radiobuttonRoundTrip;
    @FindBy(xpath = "//*[@id=\"lbl-flight-search-type-one-way\"]")
    private WebElement radiobuttonOneWay;
    //DEPARTURES
    @FindBy(css = "input[placeholder='Aeropuerto de salida']")
    private WebElement searchInputFieldForDeparture;
    @FindBy(xpath = "//*[@id=\"search-container\"]/div[1]/div/form/div[2]/div/div/div[1]/div[3]/div/div/div[2]/popup-content/core-linked-list/div[1]")
    private WebElement departureCountries;
    @FindBy(xpath = "//*[@id=\"search-container\"]/div[1]/div/form/div[2]/div[1]/div/div[1]/div[3]/div/div/div[2]/popup-content/core-linked-list/div[2]")
    private WebElement departureCities;
    //ARRRIVALS
    @FindBy(css = "input[placeholder='Aeropuerto de destino']")
    private WebElement searchInputFieldForArrival;
    @FindBy(xpath = "//*[@id=\"search-container\"]/div[1]/div/form/div[2]/div/div/div[3]/div[3]/div/div/div[2]/popup-content/core-linked-list/div[1]")
    private WebElement arrivalCountries;
    @FindBy(xpath = "//*[@id=\"search-container\"]/div[1]/div/form/div[2]/div[1]/div/div[3]/div[3]/div/div/div[2]/popup-content/core-linked-list/div[2]")
    private WebElement arrivalCities;
    //DEPARTURE DATE
    @FindBy(css = "input[aria-label='Vuelo de ida: - DD']")
    private WebElement searchInputFieldForDepartureDateDay;
    @FindBy(css = "input[aria-label='Vuelo de ida: - MM']")
    private WebElement searchInputFieldForDepartureDateMonth;
    @FindBy(css = "input[aria-label='Vuelo de ida: - YYYY']")
    private WebElement searchInputFieldForDepartureDateYear;
    //ARRIVAL DATE
    @FindBy(css = "input[aria-label='Vuelo de vuelta: - DD']")
    private WebElement searchInputFieldForArrivalDateDay;
    @FindBy(css = "input[aria-label='Vuelo de vuelta: - MM']")
    private WebElement searchInputFieldForArrivalDateMonth;
    @FindBy(css = "input[aria-label='Vuelo de vuelta: - YYYY']")
    private WebElement searchInputFieldForArrivalDateYear;
    //CALENDAR POPUP
    @FindBy(xpath = "//*[@id=\"row-dates-pax\"]/div[1]/div/div[2]/div/div[3]/div/div/div[2]/popup-content/core-datepicker")
    private WebElement calendarArrivalDate;
    @FindBy(xpath = "//*[@id=\"row-dates-pax\"]/div[1]/div/div[1]/div/div[3]/div/div/div[2]/popup-content/core-datepicker")
    private WebElement calendarDepartureDate;
    @FindBy(xpath = "//*[@id=\"row-dates-pax\"]/div[1]/div/div[1]/div/div[3]/div/div/div[2]/popup-content/core-datepicker/div/div[2]/button[1]")
    private WebElement calendarLeftArrowDepartures;
    @FindBy(xpath = "//*[@id=\"row-dates-pax\"]/div[1]/div/div[2]/div/div[3]/div/div/div[2]/popup-content/core-datepicker/div/div[2]/button[1]")
    private WebElement calendarLeftArrowArrivals;
    @FindBy(xpath = "//*[@id=\"row-dates-pax\"]/div[1]/div/div[1]/div/div[3]/div/div/div[2]/popup-content/core-datepicker/div/div[2]/button[2]")
    private WebElement calendarRightArrowDepartures;
    @FindBy(xpath = "//*[@id=\"row-dates-pax\"]/div[1]/div/div[2]/div/div[3]/div/div/div[2]/popup-content/core-datepicker/div/div[2]/button[2]")
    private WebElement calendarRightArrowArrivals;
    //PASSENGERS
    @FindBy(xpath = "//*[@id=\"row-dates-pax\"]/div[2]/div[2]/div[2]/div/div[1]")
    private WebElement addPassengers;
    @FindBy(xpath = "//*[@id=\"row-dates-pax\"]/div[2]/div[3]/div/div/div[2]/popup-content/div[6]/div/div[3]/core-inc-dec/button[2]")
    private WebElement addAnAdult;
    @FindBy(xpath = "//*[@id=\"row-dates-pax\"]/div[2]/div[3]/div/div/div[2]/popup-content/div[6]/div/div[3]/core-inc-dec/button[1]")
    private WebElement subAnAdult;
    @FindBy(xpath = "//*[@id=\"row-dates-pax\"]/div[2]/div[3]/div/div/div[2]/popup-content/div[7]/div/div[3]/core-inc-dec/button[2]")
    private WebElement addATeen;
    @FindBy(xpath = "//*[@id=\"row-dates-pax\"]/div[2]/div[3]/div/div/div[2]/popup-content/div[8]/div/div[3]/core-inc-dec/button[2]")
    private WebElement addAKid;
    @FindBy(xpath = "//*[@id=\"row-dates-pax\"]/div[2]/div[3]/div/div/div[2]/popup-content/div[9]/div/div[3]/core-inc-dec/button[2]")
    private WebElement addABaby;
    @FindBy (xpath = "//*[@id=\"row-dates-pax\"]/div[2]/div[3]/div/div/div[2]/popup-content/div[5]")
    private WebElement sameNumberAlert;
    //INFANT POP-UP
    @FindBy(xpath = "//*[@id=\"ngdialog1\"]/div[2]/div[1]/div/div")
    private WebElement infantPopUp;
    @FindBy(xpath = "//*[@id=\"ngdialog1\"]/div[2]/div[1]/div/div/div[2]/div/dialog-body/div/button")
    private WebElement closeInfantPopUp;
    //TERMS AND SUBMIT
    @FindBy(xpath = "//*[@id=\"search-container\"]/div[1]/div/div/div[2]/div/label")
    private WebElement clickTerms;
    @FindBy(xpath = "//*[@id=\"search-container\"]/div[1]/div/form/div[4]/button[2]")
    private WebElement clickSubmit;
    //GOING TO THE FLIGHTS TABLE WEBPAGE
    @FindBy(xpath = "/html/body/rooms-root/rooms-nav-header/div/div/div[1]/rooms-home-link/a/img")
    private WebElement ryanairRoomsLogo;
    @FindBy(xpath = "//*[@id=\"menu-container\"]/a/core-icon/div/svg")
    private WebElement ryanairLogo;
    @FindBy(css = "@class='direct'")
    private WebElement directFlight;


    //CONSTRUCTOR
    public RyanairSearchPage(WebDriver driver) {
        super(driver);
        max(driver);
    }

    public static void max(WebDriver driver){
        driver.manage().window().maximize();
    }

    @WhenPageOpens
    public void waitUntilRyanairLogoAppears() {
        $("#home").waitUntilVisible();
    }


    public void closeCookies() {
        element(closePopUp).waitUntilClickable();
        element(closePopUp).click();
        js.executeScript("window.scrollTo(0,400);");
    }

    public void clickOnOneWay() {
        element(radiobuttonOneWay).waitUntilClickable();
        element(radiobuttonOneWay).click();
    }

    public void clickOnRoundTrip() {
        element(radiobuttonRoundTrip).waitUntilClickable();
        element(radiobuttonRoundTrip).click();
    }

    public void searchForDeparture(String searchRequest) {
        element(searchInputFieldForDeparture).waitUntilEnabled();
        element(searchInputFieldForDeparture).clear();
        element(searchInputFieldForDeparture).type(searchRequest);
    }

    public void searchForConcreteDeparture(String searchRequest1, String searchRequest2) {
        element(searchInputFieldForDeparture).waitUntilEnabled();
        element(searchInputFieldForDeparture).clear();
        element(searchInputFieldForDeparture).click();
        element(departureCountries).waitUntilEnabled();
        WebElement search1 = dr.findElement(By.xpath("//div[text()[contains(.,'"+searchRequest1+"')]]"));
        element(search1).click();
        element(departureCities).waitUntilEnabled();
        WebElement search2 = dr.findElement(By.xpath("//div/span[text()[contains(.,'"+searchRequest2+"')]]"));
        element(search2).click();
        //*[text()[contains(.,'ABC')]]
    }

    public void searchForArrival(String searchRequest) {
        element(searchInputFieldForArrival).waitUntilEnabled();
        element(searchInputFieldForArrival).clear();
        element(searchInputFieldForArrival).typeAndEnter(searchRequest);
    }

    public void searchForConcreteArrival(String searchRequest1, String searchRequest2) {
        element(arrivalCountries).waitUntilEnabled();
        WebElement search1 = dr.findElement(By.xpath("//div[text()[contains(.,'"+searchRequest1+"')]]"));
        element(search1).click();
        element(arrivalCities).waitUntilEnabled();
        WebElement search2 = dr.findElement(By.xpath("//div/span[text()[contains(.,'"+searchRequest2+"')]]"));
        element(search2).click();
        //*[text()[contains(.,'ABC')]]
    }
    //This method only works on a concrete scenario
    public void searchForDepartureDate() {
        GregorianCalendar fecha = new GregorianCalendar();
        fecha.add(Calendar.DAY_OF_MONTH, 2);
        String day = Integer.toString(fecha.get(fecha.DATE));
        String month = Integer.toString(fecha.get(fecha.MONTH)+1);
        String year = Integer.toString(fecha.get(fecha.YEAR));

        element(searchInputFieldForDepartureDateDay).waitUntilEnabled();
        element(searchInputFieldForDepartureDateDay).clear();
        element(searchInputFieldForDepartureDateDay).click();
        element(searchInputFieldForDepartureDateDay).type(day);
        element(searchInputFieldForDepartureDateMonth).click();
        element(searchInputFieldForDepartureDateMonth).type(month);
        element(searchInputFieldForDepartureDateYear).click();
        element(searchInputFieldForDepartureDateYear).type(year);
    }

    //This method is for typing the date instead of clicking or searching (only works with actual month)
    public void searchForConcreteDepartureDate(String searchRequest) {
        String day = searchRequest.substring(8,10);
        String month = searchRequest.substring(5,7);
        String year = searchRequest.substring(0,4);
        element(searchInputFieldForDepartureDateDay).waitUntilEnabled();
        element(searchInputFieldForDepartureDateDay).clear();
        element(searchInputFieldForDepartureDateDay).click();
        element(searchInputFieldForDepartureDateDay).type(day);
        element(searchInputFieldForDepartureDateMonth).click();
        element(searchInputFieldForDepartureDateMonth).type(month);
        element(searchInputFieldForDepartureDateYear).click();
        element(searchInputFieldForDepartureDateYear).type(year);
    }

    //This method is for typing the date instead of clicking or searching (only works with actual month)
    public void searchForConcreteArrivalDate(String searchRequest) {
        String day = searchRequest.substring(8,10);
        String month = searchRequest.substring(5,7);
        String year = searchRequest.substring(0,4);
        element(searchInputFieldForArrivalDateDay).waitUntilEnabled();
        element(searchInputFieldForArrivalDateDay).clear();
        element(searchInputFieldForArrivalDateDay).click();
        element(searchInputFieldForArrivalDateDay).type(day);
        element(searchInputFieldForArrivalDateMonth).click();
        element(searchInputFieldForArrivalDateMonth).type(month);
        element(searchInputFieldForArrivalDateYear).click();
        element(searchInputFieldForArrivalDateYear).type(year);
    }

    public void searchForConcreteDepartureDateByClick(String searchRequest) {
        String day = searchRequest.substring(8,10);
        String month = searchRequest.substring(5,7);
        String year = searchRequest.substring(0,4);
        String dateInverted = day+"-"+month+"-"+year;
        element(searchInputFieldForDepartureDateDay).waitUntilEnabled();
        boolean present = false;
        while(!present) {
            element(calendarDepartureDate).waitUntilEnabled().waitUntilClickable();
            List<WebElement> isPresent = dr.findElements(By.xpath("//li[@data-id='"+dateInverted+"']"));
            if (isPresent.size() != 0) {
                WebElement searchDate = dr.findElement(By.xpath("//li[@data-id='"+dateInverted+"']"));
                element(searchDate).waitUntilEnabled().waitUntilClickable();
                element(searchDate).click();
                present = true;
            }
            else {
                element(calendarRightArrowDepartures).waitUntilEnabled();
                element(calendarRightArrowDepartures).click();
                present = false;
            }

        }
    }

    public void searchForConcreteArrivalDateByClick(String searchRequest) {
        String day = searchRequest.substring(8,10);
        String month = searchRequest.substring(5,7);
        String year = searchRequest.substring(0,4);
        String dateInverted = day+"-"+month+"-"+year;
        element(searchInputFieldForArrivalDateDay).waitUntilEnabled();
        boolean present = false;
        while(!present) {
            element(calendarArrivalDate).waitUntilEnabled().waitUntilClickable();
            List<WebElement> isPresent = dr.findElements(By.xpath("//li[@data-id='"+dateInverted+"']"));
            if (isPresent.size() != 0) {
                WebElement searchDate = dr.findElement(By.xpath("//li[@data-id='"+dateInverted+"']"));
                element(searchDate).waitUntilEnabled().waitUntilClickable();
                element(searchDate).click();
                present = true;
            }
            else {
                element(calendarRightArrowArrivals).waitUntilEnabled();
                element(calendarRightArrowArrivals).click();
                present = false;
            }

        }
    }

    public void setTwoAdults() {
        element(addPassengers).waitUntilClickable();
        element(addPassengers).click();
        element(addAnAdult).waitUntilClickable();
        element(addAnAdult).click();
        element(addPassengers).click();
    }

    //This one it's only for adding passengers
    public void setPassengers(int adults, int teens, int kids, int babies) {
        element(addPassengers).waitUntilClickable();
        element(addPassengers).click();
        if(adults>1) {
            element(addAnAdult).waitUntilClickable();
            for(int i=1; i<adults; ++i) {
                element(addAnAdult).click();
            }
        }
        if(teens>0) {
            element(addATeen).waitUntilClickable();
            for(int i=0; i<teens; ++i) {
                element(addATeen).click();
            }
        }
        if(kids>0) {
            element(addAKid).waitUntilClickable();
            for(int i=0; i<kids; ++i) {
                element(addAKid).click();
            }
        }
        if(babies>0) {
            element(addABaby).waitUntilClickable();
            for (int i = 0; i < babies; ++i) {
                if (i == 0) {
                    element(addABaby).click();
                    element(infantPopUp).waitUntilVisible();
                    element(closeInfantPopUp).waitUntilClickable();
                    element(closeInfantPopUp).click();
                    element(infantPopUp).waitUntilNotVisible();
                    element(addABaby).waitUntilVisible();
                    element(addABaby).waitUntilClickable();
                } else {
                    element(addABaby).click();
                }
            }
        }
    }

    public void setSubmit() {
        element(clickTerms).waitUntilClickable();
        element(clickTerms).click();
        element(clickSubmit).waitUntilClickable();
        element(clickSubmit).click();
    }

    public void setChangeTheTab() {
        element(ryanairRoomsLogo).waitUntilClickable();
        Set<String> handles = dr.getWindowHandles(); // Gets all the available windows
        for(String handle : handles) {
            // switching back to each window in loop
            dr.switchTo().window(handle);
            if (dr.getTitle().equals("Ryanair")) {
            } // Compare title and if title matches stop loop and return true
        }
    }

    public static boolean isDisplayed(WebElement element) {
        try {
            if (element.isDisplayed())
                return element.isDisplayed();
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    public void setCloseBrowser() {
        dr.close();
        dr.close();
    }
}