package com.serenitySteps;

import com.page.MaquillaliaMain;
import net.thucydides.core.annotations.Step;

/**
 * Created by operador on 14/12/2016.
 */
public class MaquillaliaMainSteps {

    MaquillaliaMain mainPage;
    @Step
    public void openMaquillaliaPage() {
        mainPage.open();
    }

    @Step
    public void closeNewsletterPopUp() {
        mainPage.closeNewsletterPopUp();
    }

    @Step
    public void fillNewsletterForm(String name, String email) {
        mainPage.fillNewsletterForm(name, email);
    }

//    @Step
//    public void verifyResult() {
//        Assert.assertTrue("There are no available flights for that date. " , mainPage.verifyFlightsAvailable());
//    }
//
//    @Step
//    public void verifyResult2() {
//        Assert.assertTrue("No error message has appeared" , mainPage.verifyBabyErrorMessage());
//    }

    @Step public void closeWebBrowser() {
        mainPage.setCloseBrowser();
    }
}
