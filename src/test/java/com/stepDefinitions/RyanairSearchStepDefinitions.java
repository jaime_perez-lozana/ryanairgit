package com.stepDefinitions;

import com.serenitySteps.RyanairSearchSteps;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.openqa.selenium.WebDriver;

/**
 * Created by operador on 14/12/2016.
 */
public class RyanairSearchStepDefinitions {

    @Steps
    RyanairSearchSteps ryanairSearchSteps;

    @Given("^I want to search a flight in Ryanair$")
    public void iWantToSearchInRyanair() throws Throwable {
        ryanairSearchSteps.openRyanairSearchPage();
    }

    @When("^I close the cookies message$")
    public void iCloseCookiesMessage() throws Throwable {
        ryanairSearchSteps.closeCookiesMessage();
    }

    @And("^I select one-way$")
    public void iClickOneWay() throws Throwable {
        ryanairSearchSteps.clickOnOneWay();
    }

    @And("^I select round trip$")
    public void iClickRoundTrip() throws Throwable {
        ryanairSearchSteps.clickOnRoundTrip();
    }

    @And("^I set the Departure airport as '(.*)'$")
    public void iSearchDepartureFor(String searchRequest) throws Throwable {
        ryanairSearchSteps.searchForDeparture(searchRequest);
    }

    @And("^I select from '(.*)' '(.*)' to take off$")
    public void iSearchConcreteDepartureFor(String searchRequest1, String searchRequest2) throws Throwable {
        ryanairSearchSteps.searchForConcreteDeparture(searchRequest1, searchRequest2);
    }

    @And("^I set the Arrival airport as '(.*)'$")
    public void iSearchArrivalFor(String searchRequest) throws Throwable {
        ryanairSearchSteps.searchForArrival(searchRequest);
    }

    @And("^I select '(.*)' '(.*)' to land$")
    public void iSearchConcreteArrivalFor(String searchRequest1, String searchRequest2) throws Throwable {
        ryanairSearchSteps.searchForConcreteArrival(searchRequest1, searchRequest2);
    }

    @And("^I set the Departure Date$")
    public void iSearchDepartureDateFor() throws Throwable {
        ryanairSearchSteps.searchForDepartureDate();
    }

    @And("^I select departure date '(.*)'$")
    public void iSearchConcreteDepartureDateFor(String searchRequest) throws Throwable {
        ryanairSearchSteps.searchForConcreteDepartureDate(searchRequest);
    }

    @And("^I select departure date by clicking '(.*)'$")
    public void iSearchConcreteDepartureDateByClick(String searchRequest) throws Throwable {
        ryanairSearchSteps.searchForConcreteDepartureDateByClick(searchRequest);
    }

    @And("^I select arrival date by clicking '(.*)'$")
    public void iSearchConcreteArrivalDateByClick(String searchRequest) throws Throwable {
        ryanairSearchSteps.searchForConcreteArrivalDateByClick(searchRequest);
    }

    @And("^I select arrival date '(.*)'$")
    public void iSearchConcreteArrivalDateFor(String searchRequest) throws Throwable {
        ryanairSearchSteps.searchForConcreteArrivalDate(searchRequest);
    }

    @And("^I click on the passengers dropdown and I select 2 adults$")
    public void iSetTwoAdults() throws Throwable {
        ryanairSearchSteps.setTwoAdults();
    }

    @And("^I set the passengers with '(.*)' '(.*)' '(.*)' '(.*)'$")
    public void iSetPassengers(int adults, int teens, int kids, int babies) throws Throwable {
        ryanairSearchSteps.setPassengers(adults, teens, kids, babies);
    }

    @And("^Submit to the next page$")
    public void iClickOnSubmit() throws Throwable {
        ryanairSearchSteps.setSubmit();
    }

    @And("^I change the tab to the Ryanair flights table$")
    public void iChangeTheTab() throws Throwable {
        ryanairSearchSteps.setChangeTheTab();
    }

    @Then("^I should see at least one flight$")
    public void iShouldSeeLinkTo() throws Throwable {
        ryanairSearchSteps.verifyResult();
    }

    @Then("^An error should appear on the passengers dropdown$")
    public void iShouldSeeTheBabiesError() throws Throwable {
        ryanairSearchSteps.verifyResult2();
    }

    @And("^I close the browser$")
    public void iCloseTheBrowser() throws Throwable {
        ryanairSearchSteps.setCloseBrowser();
    }

}
