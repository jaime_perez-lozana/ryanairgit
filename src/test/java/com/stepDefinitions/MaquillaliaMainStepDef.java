package com.stepDefinitions;

import com.serenitySteps.MaquillaliaMainSteps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

/**
 * Created by operador on 14/12/2016.
 */
public class MaquillaliaMainStepDef {

    @Steps
    MaquillaliaMainSteps maquillaliaMainSteps;

    @Given("^I have the web fully loaded$")
    public void iWantToOpenMaquillalia() throws Throwable {
        maquillaliaMainSteps.openMaquillaliaPage();
    }

    @When("^I close the Newsletter pop up$")
    public void iCloseNewsletterPopUp() throws Throwable {
        maquillaliaMainSteps.closeNewsletterPopUp();
    }

    @And("^I write a name '(.*)' and an email '(.*)'$")
    public void iSearchConcreteArrivalFor(String searchRequest1, String searchRequest2) throws Throwable {
        maquillaliaMainSteps.fillNewsletterForm(searchRequest1, searchRequest2);
    }

    @And("^I close the web browser$")
    public void iCloseTheWebBrowser() throws Throwable {
        maquillaliaMainSteps.closeWebBrowser();
    }
}
