package com.serenitySteps;

import com.page.RyanairResultsPage;
import com.page.RyanairSearchPage;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;

/**
 * Created by operador on 14/12/2016.
 */
public class RyanairSearchSteps {

    RyanairSearchPage searchPage;
    RyanairResultsPage resultsPage;
    @Step
    public void openRyanairSearchPage() {
        searchPage.open();
    }

    @Step
    public void closeCookiesMessage() { searchPage.closeCookies(); }

    @Step
    public void clickOnOneWay() { searchPage.clickOnOneWay(); }

    @Step
    public void clickOnRoundTrip() { searchPage.clickOnRoundTrip(); }

    @Step
    public void searchForDeparture(String searchRequest) {
        searchPage.searchForDeparture(searchRequest);
    }

    @Step
    public void searchForConcreteDeparture(String searchRequest1, String searchRequest2) {
        searchPage.searchForConcreteDeparture(searchRequest1, searchRequest2);
    }

    @Step
    public void searchForArrival(String searchRequest) {
        searchPage.searchForArrival(searchRequest);
    }

    @Step
    public void searchForConcreteArrival(String searchRequest1, String searchRequest2) {
        searchPage.searchForConcreteArrival(searchRequest1, searchRequest2);
    }

    @Step
    public void searchForDepartureDate() {
        searchPage.searchForDepartureDate();
    }

    @Step
    public void searchForConcreteDepartureDate(String searchRequest) {
        searchPage.searchForConcreteDepartureDate(searchRequest);
    }

    @Step
    public void searchForConcreteDepartureDateByClick(String searchRequest) {
        searchPage.searchForConcreteDepartureDateByClick(searchRequest);
    }

    @Step
    public void searchForConcreteArrivalDate(String searchRequest) {
        searchPage.searchForConcreteArrivalDate(searchRequest);
    }

    @Step
    public void searchForConcreteArrivalDateByClick(String searchRequest) {
        searchPage.searchForConcreteArrivalDateByClick(searchRequest);
    }

    @Step
    public void setTwoAdults() {
        searchPage.setTwoAdults();
    }

    @Step public void setPassengers(int adults, int teens, int kids, int babies) {
        searchPage.setPassengers(adults, teens, kids, babies);
    }

    @Step
    public void setSubmit() {
        searchPage.setSubmit();
    }

    @Step
    public void setChangeTheTab() {
        searchPage.setChangeTheTab();
    }

    @Step
    public void verifyResult() {
        Assert.assertTrue("There are no available flights for that date. " , resultsPage.verifyFlightsAvailable());
    }

    @Step
    public void verifyResult2() {
        Assert.assertTrue("No error message has appeared" , resultsPage.verifyBabyErrorMessage());
    }

    @Step public void setCloseBrowser() {
        searchPage.setCloseBrowser();
    }
}
