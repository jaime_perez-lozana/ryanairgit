package com.page;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.WhenPageOpens;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by operador on 14/12/2016.
 */
public class RyanairResultsPage extends PageObject {
    //ATRIBUTOS
    @CacheLookup
    @FindBy(css = ".direct")
    private WebElement directFlight;
    @FindBy(css = ".flights-table")
    private WebElement flightsTable;
    @FindBy (xpath = "//*[@id=\"row-dates-pax\"]/div[2]/div[3]/div/div/div[2]/popup-content/div[5]")
    private WebElement sameNumberAlert;

    //CONSTRUCTOR
    public RyanairResultsPage(WebDriver driver) {
        super(driver);

    }

    @WhenPageOpens
    public void waitUntilRyanairLogoAppears() {
        $(".flights-table").waitUntilVisible();
    }

    public boolean verifyFlightsAvailable () {
        while (!isDisplayed(directFlight)) {
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                System.out.println(e);
            }
        }

        return true;
    }

    public boolean verifyBabyErrorMessage () {
        while (!isDisplayed(sameNumberAlert)) {
            try {
                System.out.println("I don't see the infants alert");
            } catch (Exception e) {
                System.out.println(e);
            }
        }
        return true;
    }

    public static boolean isDisplayed(WebElement element) {
        try {
            if (element.isDisplayed())
                return element.isDisplayed();
        } catch (Exception e) {
            return false;
        }
        return false;
    }

}