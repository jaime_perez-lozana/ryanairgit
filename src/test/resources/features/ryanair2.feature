Feature: Trying set a flight on Ryanair with various conditions
  Scenario Outline: Check that I cannot look for a flight without enough adults
    Given I want to search a flight in Ryanair
    When I close the cookies message
    And I select one-way
    And I select from '<departureCountry>' '<departureCity>' to take off
    And I select '<arrivalCountry>' '<arrivalCity>' to land
    And I select departure date '<yyyy-mm-dd>'
    And I set the passengers with '<adults>' '<teens>' '<kids>' '<babies>'
    Then An error should appear on the passengers dropdown
    And I close the browser
    Examples:
      | departureCountry  | departureCity | arrivalCountry  | arrivalCity       | yyyy-mm-dd  | adults | teens | kids | babies |
      | Francia           | Brive         | Reino Unido     | Londres Stansted  | 2018-08-23  | 1      | 0     | 0    | 2      |

  Scenario Outline: Looking for a flight on Ryanair with concrete parameters
    Given I want to search a flight in Ryanair
    When I close the cookies message
    And I select one-way
    And I select from '<departureCountry>' '<departureCity>' to take off
    And I select '<arrivalCountry>' '<arrivalCity>' to land
    And I select departure date '<yyyy-mm-dd>'
    And I set the passengers with '<adults>' '<teens>' '<kids>' '<babies>'
    And Submit to the next page
    And I change the tab to the Ryanair flights table
    Then I should see at least one flight
    And I close the browser
    Examples:
      | departureCountry  | departureCity | arrivalCountry  | arrivalCity       | yyyy-mm-dd  | adults | teens | kids | babies |
      | Alemania          | Bremen        | Suecia          | Estocolmo-Skavsta | 2018-08-25  | 2      | 0     | 2    | 0      |

  Scenario Outline: Test a round flight with dates in different month and not visible months
    Given I want to search a flight in Ryanair
    When I close the cookies message
    And I select round trip
    And I select from '<departureCountry>' '<departureCity>' to take off
    And I select '<arrivalCountry>' '<arrivalCity>' to land
    And I select departure date by clicking '<departure_yyyy-mm-dd>'
    And I select arrival date by clicking '<return_yyyy-mm-dd>'
    And I set the passengers with '<adults>' '<teens>' '<kids>' '<babies>'
    And Submit to the next page
    And I change the tab to the Ryanair flights table
    Then I should see at least one flight
    And I close the browser
  Examples:
    | departureCountry | departureCity | arrivalCountry | arrivalCity | departure_yyyy-mm-dd | return_yyyy-mm-dd | adults | teens | kids | babies |
    | Serbia           | Nis           | Eslovaquia     | Bratislava  | 2018-10-26           | 2018-11-12        | 2      | 0     | 0    | 0      |