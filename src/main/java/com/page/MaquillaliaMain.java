package com.page;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.annotations.WhenPageOpens;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;

import java.util.concurrent.TimeUnit;

/**
 * Created by operador on 14/12/2016.
 */

@DefaultUrl("https://www.maquillalia.com/")
public class MaquillaliaMain extends PageObject {

    JavascriptExecutor js = (JavascriptExecutor) this.getDriver();
    WebDriver dr = this.getDriver();

    //ATTRIBUTES
    @CacheLookup
    //NEWSLETTER POP-UP
    @FindBy(css = "input[title='close form']")
    private WebElement closePopUp;
    @FindBy(xpath = "//*[@id=\"wrapper\"]/a/img")
    private WebElement popUpLogo;
    @FindBy(xpath = "//*[@id=\"wrapper\"]/div")
    private WebElement popUpForm;


    //CONSTRUCTOR
    public MaquillaliaMain(WebDriver driver) {
        super(driver);
        max(driver);
    }

    public static void max(WebDriver driver) {
        driver.manage().window().maximize();
    }

    @WhenPageOpens
    public void waitUntilMaquillaliaPopUpAppears() {
        $("//*[@id=\"popupContainer\"]").waitUntilVisible();
    }

    public void closeNewsletterPopUp() {
//        if (element(closePopUp).isVisible()) {
//            element(closePopUp).waitUntilClickable();
//            element(closePopUp).click();
//        }

        dr.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);

        if (isDisplayed(closePopUp)) {
            System.out.println("Found it!");
        }

        try {
            Thread.sleep(5000);
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void fillNewsletterForm(String name, String email) {
        element(popUpForm).waitUntilVisible();
    }

//    public void searchForDeparture(String searchRequest) {
//        element(searchInputFieldForDeparture).waitUntilEnabled();
//        element(searchInputFieldForDeparture).clear();
//        element(searchInputFieldForDeparture).type(searchRequest);
//    }
//
//    public void searchForConcreteDeparture(String searchRequest1, String searchRequest2) {
//        element(searchInputFieldForDeparture).waitUntilEnabled();
//        element(searchInputFieldForDeparture).clear();
//        element(searchInputFieldForDeparture).click();
//        element(departureCountries).waitUntilEnabled();
//        WebElement search1 = dr.findElement(By.xpath("//div[text()[contains(.,'"+searchRequest1+"')]]"));
//        element(search1).click();
//        element(departureCities).waitUntilEnabled();
//        WebElement search2 = dr.findElement(By.xpath("//div/span[text()[contains(.,'"+searchRequest2+"')]]"));
//        element(search2).click();
//        //*[text()[contains(.,'ABC')]]
//    }
//
//    public void searchForArrival(String searchRequest) {
//        element(searchInputFieldForArrival).waitUntilEnabled();
//        element(searchInputFieldForArrival).clear();
//        element(searchInputFieldForArrival).typeAndEnter(searchRequest);
//    }
//
//    public void searchForConcreteArrival(String searchRequest1, String searchRequest2) {
//        element(arrivalCountries).waitUntilEnabled();
//        WebElement search1 = dr.findElement(By.xpath("//div[text()[contains(.,'"+searchRequest1+"')]]"));
//        element(search1).click();
//        element(arrivalCities).waitUntilEnabled();
//        WebElement search2 = dr.findElement(By.xpath("//div/span[text()[contains(.,'"+searchRequest2+"')]]"));
//        element(search2).click();
//    }
//    //This method only works on a concrete scenario
//    public void searchForDepartureDate() {
//        GregorianCalendar fecha = new GregorianCalendar();
//        fecha.add(Calendar.DAY_OF_MONTH, 2);
//        String day = Integer.toString(fecha.get(fecha.DATE));
//        String month = Integer.toString(fecha.get(fecha.MONTH)+1);
//        String year = Integer.toString(fecha.get(fecha.YEAR));
//
//        element(searchInputFieldForDepartureDateDay).waitUntilEnabled();
//        element(searchInputFieldForDepartureDateDay).clear();
//        element(searchInputFieldForDepartureDateDay).click();
//        element(searchInputFieldForDepartureDateDay).type(day);
//        element(searchInputFieldForDepartureDateMonth).click();
//        element(searchInputFieldForDepartureDateMonth).type(month);
//        element(searchInputFieldForDepartureDateYear).click();
//        element(searchInputFieldForDepartureDateYear).type(year);
//    }
//
//    //This method is for typing the date instead of clicking or searching (only works with actual month)
//    public void searchForConcreteDepartureDate(String searchRequest) {
//        String day = searchRequest.substring(8,10);
//        String month = searchRequest.substring(5,7);
//        String year = searchRequest.substring(0,4);
//        element(searchInputFieldForDepartureDateDay).waitUntilEnabled();
//        element(searchInputFieldForDepartureDateDay).clear();
//        element(searchInputFieldForDepartureDateDay).click();
//        element(searchInputFieldForDepartureDateDay).type(day);
//        element(searchInputFieldForDepartureDateMonth).click();
//        element(searchInputFieldForDepartureDateMonth).type(month);
//        element(searchInputFieldForDepartureDateYear).click();
//        element(searchInputFieldForDepartureDateYear).type(year);
//    }
//
//    //This method is for typing the date instead of clicking or searching (only works with actual month)
//    public void searchForConcreteArrivalDate(String searchRequest) {
//        String day = searchRequest.substring(8,10);
//        String month = searchRequest.substring(5,7);
//        String year = searchRequest.substring(0,4);
//        element(searchInputFieldForArrivalDateDay).waitUntilEnabled();
//        element(searchInputFieldForArrivalDateDay).clear();
//        element(searchInputFieldForArrivalDateDay).click();
//        element(searchInputFieldForArrivalDateDay).type(day);
//        element(searchInputFieldForArrivalDateMonth).click();
//        element(searchInputFieldForArrivalDateMonth).type(month);
//        element(searchInputFieldForArrivalDateYear).click();
//        element(searchInputFieldForArrivalDateYear).type(year);
//    }
//
//    public void searchForConcreteDepartureDateByClick(String searchRequest) {
//        String day = searchRequest.substring(8,10);
//        String month = searchRequest.substring(5,7);
//        String year = searchRequest.substring(0,4);
//        String dateInverted = day+"-"+month+"-"+year;
//        element(searchInputFieldForDepartureDateDay).waitUntilEnabled();
//        boolean present = false;
//        while(!present) {
//            element(calendarDepartureDate).waitUntilEnabled().waitUntilClickable();
//            List<WebElement> isPresent = dr.findElements(By.xpath("//li[@data-id='"+dateInverted+"']"));
//            if (isPresent.size() != 0) {
//                WebElement searchDate = dr.findElement(By.xpath("//li[@data-id='"+dateInverted+"']"));
//                element(searchDate).waitUntilEnabled().waitUntilClickable();
//                element(searchDate).click();
//                present = true;
//            }
//            else {
//                element(calendarRightArrowDepartures).waitUntilEnabled();
//                element(calendarRightArrowDepartures).click();
//                present = false;
//            }
//
//        }
//    }
//
//    public void searchForConcreteArrivalDateByClick(String searchRequest) {
//        String day = searchRequest.substring(8,10);
//        String month = searchRequest.substring(5,7);
//        String year = searchRequest.substring(0,4);
//        String dateInverted = day+"-"+month+"-"+year;
//        element(searchInputFieldForArrivalDateDay).waitUntilEnabled();
//        boolean present = false;
//        while(!present) {
//            element(calendarArrivalDate).waitUntilEnabled().waitUntilClickable();
//            List<WebElement> isPresent = dr.findElements(By.xpath("//li[@data-id='"+dateInverted+"']"));
//            if (isPresent.size() != 0) {
//                WebElement searchDate = dr.findElement(By.xpath("//li[@data-id='"+dateInverted+"']"));
//                element(searchDate).waitUntilEnabled().waitUntilClickable();
//                element(searchDate).click();
//                present = true;
//            }
//            else {
//                element(calendarRightArrowArrivals).waitUntilEnabled();
//                element(calendarRightArrowArrivals).click();
//                present = false;
//            }
//
//        }
//    }
//
//    public void setTwoAdults() {
//        element(addPassengers).waitUntilClickable();
//        element(addPassengers).click();
//        element(addAnAdult).waitUntilClickable();
//        element(addAnAdult).click();
//        element(addPassengers).click();
//    }
//
//    //This one it's only for adding passengers
//    public void setPassengers(int adults, int teens, int kids, int babies) {
//        element(addPassengers).waitUntilClickable();
//        element(addPassengers).click();
//        if(adults>1) {
//            element(addAnAdult).waitUntilClickable();
//            for(int i=1; i<adults; ++i) {
//                element(addAnAdult).click();
//            }
//        }
//        if(teens>0) {
//            element(addATeen).waitUntilClickable();
//            for(int i=0; i<teens; ++i) {
//                element(addATeen).click();
//            }
//        }
//        if(kids>0) {
//            element(addAKid).waitUntilClickable();
//            for(int i=0; i<kids; ++i) {
//                element(addAKid).click();
//            }
//        }
//        if(babies>0) {
//            element(addABaby).waitUntilClickable();
//            for (int i = 0; i < babies; ++i) {
//                if (i == 0) {
//                    element(addABaby).click();
//                    element(infantPopUp).waitUntilVisible();
//                    element(closeInfantPopUp).waitUntilClickable();
//                    element(closeInfantPopUp).click();
//                    element(infantPopUp).waitUntilNotVisible();
//                    element(addABaby).waitUntilVisible();
//                    element(addABaby).waitUntilClickable();
//                } else {
//                    element(addABaby).click();
//                }
//            }
//        }
//    }
//
//    public void setSubmit() {
//        element(clickTerms).waitUntilClickable();
//        element(clickTerms).click();
//        element(clickSubmit).waitUntilClickable();
//        element(clickSubmit).click();
//    }
//
//    public void setChangeTheTab() {
//        element(ryanairRoomsLogo).waitUntilClickable();
//        Set<String> handles = dr.getWindowHandles(); // Gets all the available windows
//        for(String handle : handles) {
//            // switching back to each window in loop
//            dr.switchTo().window(handle);
//            if (dr.getTitle().equals("Ryanair")) {
//            } // Compare title and if title matches stop loop and return true
//        }
//    }

    public static boolean isDisplayed(WebElement element) {
        try {
            if (element.isDisplayed())
                return element.isDisplayed();
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    public void setCloseBrowser() {
        dr.close();
        dr.close();
    }
}