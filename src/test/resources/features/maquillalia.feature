Feature: Maquillalia Automated

#  @run
  Scenario: Web opens fine
    Given I have the web fully loaded
    Then I close the browser

  @run
  Scenario: Close newsletter pop Up in page
    Given I have the web fully loaded
    When I close the Newsletter pop up
    Then I close the browser

#  @run
  Scenario Outline: Fill the form contained in the newsletter pop up
    Given I have the web fully loaded
    When I write a name '<name>' and an email '<email>'
#    And I click on the checkbox to accept privacy policy
#    And I submit the form
    Then I close the browser
    Examples:
      | name     | email                        |
      | Romualdo | romualdofortesting@gmail.com |