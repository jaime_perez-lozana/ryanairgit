Feature: Looking for a flight on Ryanair
  Scenario: Ryanair search
    Given I want to search a flight in Ryanair
    When I close the cookies message
    And I select one-way
    And I set the Departure airport as 'Valladolid'
    And I set the Arrival airport as 'Barcelona'
    And I set the Departure Date
    And I click on the passengers dropdown and I select 2 adults
    And Submit to the next page
    And I change the tab to the Ryanair flights table
    Then I should see at least one flight
    And I close the browser